#!/usr/bin/python
import math

'''
calcWeatherScore takes todays average temperature and the monthly average temperature
@return a score representing the percieved temperature.
Please note that the score calculation is fairly arbitrary and will likely be changed later.
Higher scores are better
'''
def calcWeatherScore(todaysAveTemp, monthlyAveTemp):

    highTemp = 18.0
    score = 0
    if (todaysAveTemp > highTemp):
        # @todo: create a better formula for percieved temperature
        score -= round(((todaysAveTemp - highTemp)/highTemp), 2)
    score += round((todaysAveTemp - monthlyAveTemp)/2, 2)

    return score
