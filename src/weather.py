#!/usr/bin/python
from datetime import timedelta, date
from canadaWeather import *
from weatherScore import calcWeatherScore


class Weather:

    def __init__(self, townCity, provCode, countryCode):
        print(townCity, provCode, countryCode)
        if countryCode == 'ca':
            normWeather = findCanadianNorm(townCity, provCode)
            if isinstance(normWeather, str):
                # Assume error
                raise ValueError("Error with findCanadianNorm, or town/city chosen not valid.")
            else:
                normWeather = round(normWeather, 2)

        # throw errors
        elif countryCode == 'us':
            print("USA weather not implimented yet.")
        else:
            return "Please move somewhere else and try again."


        self._currentWeather = findCurrentWeather(townCity, provCode)
        self._normWeather = normWeather
        self._weatherScore = calcWeatherScore(self._currentWeather, self._normWeather)

    #temp_data = (normWeather, percievedWeatherScore , rating, date.today())
    #return temp_data
