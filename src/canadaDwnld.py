#!/usr/bin/env python
# add keep searching if no data exists
import math, csv
import pandas as pd
from dataAccess import saveCanadianMonthlyData
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut
import time

# keep in mind while processing that every province is different with regards to data
def dwnldCadData(townCity, provinceCode):
    # Sometimes the method used to obtain the coordinates of a town/city fails.
    # Often the solution is as simple as waiting and then trying again.
    geoCoderTries = 0
    try:
        loc = getLatLong(townCity + ", " + provinceCode)
    except ValueError as e:
        print(e.args)


    print('downloading Canadian data for '+str(loc.latitude) + ", " + str(loc.longitude))
    climateIDList = getClimateIDList(loc.latitude, loc.longitude)

    # returns true or a string for an error
    return saveCanadianMonthlyData(climateIDList, provinceCode, townCity)


# Note that the Nominatim.geocode() method sometimes returns a timeout error
# Check for ValueError and prevent unneccecary calls to geocoder
# https://gis.stackexchange.com/questions/173569/avoid-time-out-error-nominatim-geopy-openstreetmap
def getLatLong(location, attempt=1, max_attempts=4):
    try:
        geolocator = Nominatim(user_agent="stamper@fastmail.com")
        loc = geolocator.geocode(location)
        if isinstance(loc, NonType):
            raise ValueError("Location doesn't exist")
        else:
            print("lat: "+str(loc.latitude)+", long: "+str(loc.longitude))
            return loc

    except (GeocoderTimedOut, ValueError):
        if attempt <= max_attempts:
            time.sleep(1) # Being kind to the geocoding server.
            return getLatLong(location, attempt=attempt+1)
        else:
            raise ValueError("Geocoder Failed or Invalid Location")


# takes location coordinates as input, searches a csv files, and returns a climateID
def getClimateIDList(latitude, longitude):

    latitudeCeiling = latitude
    latitudeFloor = latitude
    eastmostLongitude = longitude # east of UK is positve from 0 to 180
    westmostLongitude = longitude # moving west decreases longitude in NA
    possibleStations = []
    closestStationID = None
    closestStationIndex = 0
    df = pd.read_csv("../data/climate_station_list.csv")

    stopCounter = 0
    while len(possibleStations) == 0 and stopCounter <= 32:
        # widen search area before each search attempt
        # please note that this will fail around poles and the UK because circles
        eastmostLongitude += stopCounter # east of UK is positve from 0 to 180
        westmostLongitude -= stopCounter # moving west decreases longitude in NA
        latitudeCeiling += stopCounter
        latitudeFloor -= stopCounter
        print("start csv parse")

        i = 0
        while (i < df.shape[0] - 1):
            #print(df.iloc[i]['Latitude'])
            #print("longFloor="+str(eastmostLongitude)+", long= "+str(row['Latitude'])+", longCeiling= "+str(westmostLongitude))

            row = df.iloc[i]
            if (westmostLongitude <= float(row['Longitude']) <= eastmostLongitude):
                #print("latFloor="+str(latitudeFloor)+", lat= "+str(row['Latitude'])+", latCeiling= "+str(latitudeCeiling))
                if (latitudeFloor <= float(row['Latitude']) <= latitudeCeiling):
                # station-name, stationLongitude, stationLatitude, station-elev, CID
                    print("oy!")
                    t = row['name'], float(row['Longitude']), float(row['Latitude']), row['Climate ID']
                    print(t)
                    possibleStations.append(t)
                    #print(possibleStations)
            i+=1
        stopCounter += 1

    sortedPossible = sortStationsDistanceFrom(possibleStations, latitude, longitude)
    #cID = possibleStations[closestStationIndex][3]
    #print(cID)
    #print(possibleStations[closestStationIndex][0])
    #return cID
    return sortedPossible


def sortStationsDistanceFrom(possibleStations, latitude, longitude):
    print("sorting stations")
    # stationDist = ([station-name, stationLongitude, stationLatitude, station-elev, CID], dist)
    stationDist = []

    for station in possibleStations:
        statLon = station[1]
        statLat = station[2]
        dist = math.sqrt((longitude - statLon)**2 + (latitude - statLat)**2)
        stationDist.append((station, dist))

    stationDist.sort(key=lambda x: x[1])

    return stationDist


# Example URLS:
 #url = https://climate.weather.gc.ca/climate_data/daily_data_e.html?StationID='+stationID+'&timeframe=2&StartYear='+startYear+'&EndYear='+finYear+'&Day='+day+'&Year='+year+'&Month='+month
    #url = 'https://climate.weather.gc.ca/climate_data/bulk_data_e.html?format=xml&stationID=50430&Year=2020&Month=3&Day=1&timeframe=2&submit=Download+Data'
    #url = 'https://climate.weather.gc.ca/climate_data/bulk_data_e.html?format=csv&stationID=50430&Year=2020&Month=3&Day=1&timeframe=2&submit=Download+Data'
# example urls
#https://dd.weather.gc.ca/climate/observations/daily/csv/AB/
#https://dd.weather.gc.ca/climate/observations/monthly/csv/
#https://dd.weather.gc.ca/climate/observations/normals/csv/1981-2010/AB/
