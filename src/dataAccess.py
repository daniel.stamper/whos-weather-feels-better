#!/usr/bin/env python
import requests, io
from datetime import timedelta, date

'''Method '''
def saveCanadianMonthlyData(climateIDList, provinceCode, townCity):
    period = 'daily'

    # loop through each station ID in the climateIDList
    i=0
    while i<len(climateIDList)-1:
        # period = monthly or daily
        print(climateIDList[i])
        envCanUrls = genUrls(provinceCode, period, climateIDList[i][0][3])
        print(envCanUrls)

        # create url first
        j = len(envCanUrls)-1
        offset = 0
        while j > -1:
            print("num envCadUrls = "+str(len(envCanUrls)))
            print("j " + str(j))
            r= requests.get(envCanUrls[j][0])
            print(r.status_code)

            if (str(r.status_code) == '200'):
                s = r.content.decode('utf-8', 'ignore') # remove special characters for use in pandas dataframe
                s = s.encode('utf-8')

                with open("../data/envCadData/"+townCity+'_'+envCanUrls[0][1]+"_"+period+".csv", 'ab') as f:
                    f.write(s[offset:])

                print(r.headers['content-type'])
                print(r.encoding)

                if (j != 1):
                    return True

            # Look for the next closest weather station
            offset = 535 # this will probably break at some future time
            j-=1
        i+=1
    return "No valid urls found"


'''
Method generates the urls needed to request data from environment Canada
'''
def genUrls(provCode, period, climateID):
 
    base_url = "https://dd.weather.gc.ca/climate/observations/"

    # add more flexibility to days back size
    daysBack = 30 #note that this should be a max length of a month
    dateStart = date.today()+timedelta(days=-daysBack)
    dateEnd = date.today()

    # if days back goes into previous year
    startYear = dateStart.year
    startMonth = dateStart.month
    endMonth = dateEnd.month
    endYear = dateEnd.year

    urls = []

    # if days back goes into previous month
    yyyyE = "_" + str(endYear)
    yyyyS = "_" + str(startYear)

    if endMonth < 10:
        mmE = "-0" +str(endMonth)
    else:
        mmE = "-" + str(endMonth) # valid month range from 01 to 12

    if startMonth < 10:
        mmS = "-0" +str(startMonth)
    else:
        mmS = "-" + str(startMonth) # valid month range from 01 to 12

    url_start = base_url + period + "/csv/" + provCode + "/climate_" + period + "_" + provCode + "_" + climateID

    if (period == 'daily'):
        url_end = yyyyE + mmE + "_P1D.csv"
        url = url_start + url_end
        t = url, str(endYear)+'-'+str(endMonth)+'-'+str(dateEnd.day)
        urls.append(t)
        # Download another months data, if days back straddles two months
        if endMonth != startMonth or endYear != startYear:
            url_end = yyyyS + mmS + "_P1D.csv"
            url = url_start + url_end
            t = url, str(endYear)+'-'+str(endMonth)+'-'+str(dateEnd.day)
            urls.append(t)

    elif (period == 'monthly'):
        url_end = yyyyE + "_P1M.csv"
        url = url_start + url_end
        t = url, str(endYear)+'-'+str(endMonth)+'-'+str(dateEnd.day)
        urls.append(t)
        # Download another months data, if 30 days back straddles two months
        if endMonth != startMonth or endYear != startYear:
            url_end = yyyyS + "_P1M.csv"
            url = url_start + url_end
            t = url, str(endYear)+'-'+str(endMonth)+'-'+str(dateEnd.day)
            urls.append(t)

    return urls

