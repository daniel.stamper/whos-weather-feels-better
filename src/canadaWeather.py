#!/usr/bin/env python

import csv, os.path, requests, json
from datetime import timedelta, date

import array as arr
import pandas as pd

# remove these two dependancies
import time, datetime
from canadaDwnld import dwnldCadData
from hide import WEATHER_TOKEN

'''
Takes a file name, and a date range.
The average temperature over this date range is returned.
'''
def getTypicalTemps(fileName, dateStart, dateEnd):
    df = pd.read_csv(fileName)
    i = 0
    temps = []
    while (i < df.shape[0] - 1):
        #print(df.iloc[i]['Latitude'])
        row = df.iloc[i]
        rowDate = datetime.date(int(row['Year']), int(row['Month']), int(row['Day']))
        if rowDate >= dateStart and str(row['Mean Temp (C)']) != 'nan':
            print(str(row['Mean Temp (C)']))
            temps.append(float(row['Mean Temp (C)']))
        #print("longFloor="+str(eastmostLongitude)+", long= "+str(row['Latitude'])+", longCeiling= "+str(westmostLongitude))

        i+=1
    print(temps)
    return sum(temps)/len(temps)


''' This method determines the average temperature over the previous 30 days for a given town/city.
Assumes that a given city is located in Canada.
'''
def findCanadianNorm(townCity, provCode):
    daysBack = 30
    dateStart = date.today()+timedelta(days=-daysBack)
    dateEnd = date.today()

    yyyy = str(dateEnd.year)
    m = str(dateEnd.month)
    d = str(dateEnd.day)
    # figure out how to deal with multiple years/ months

    path = '../data/envCadData/'
    fName = path + townCity+'_'+yyyy+'-'+m+'-'+d+'_daily.csv'
    #df = pd.read_csv(fName)

    if os.path.isfile(fName) == False:
        print('Downlding Canadian Weather Data.')
        test = dwnldCadData(townCity, provCode)
        if test != True:
            return "Download Error "+str(test)

    # add error checking here
    # getTypicalTemps returns the average temperature for a Canadian location over a date range
    return getTypicalTemps(fName, dateStart, dateEnd)


def findCurrentWeather(townCity, provCode):
    city = townCity
    dataResponse = requests.get("http://api.openweathermap.org/data/2.5/weather?q="+city+"&appid="+WEATHER_TOKEN)
    #weatherDict = json.loads(str(weatherObj)

    try:
        weatherObj = dataResponse.json()

    except (ValueError, KeyError, TypeError):
        print("Json formating error")

        # convert temperature from Kelvin to Celsuis
    todaysTemp = round(weatherObj['main']['temp'] - 273.15, 2)
    #print(dataResponse)

    return todaysTemp
