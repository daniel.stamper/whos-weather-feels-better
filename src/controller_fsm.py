import logging

'''
code taken and modified from aniogram example fsm
https://github.com/aiogram/aiogram/blob/dev-2.x/examples/finite_state_machine_example.py
The code on this page is in the process of being replaced
'''
import aiogram.utils.markdown as md
from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import ParseMode
from aiogram.utils import executor

from weather import Weather
from rankCodes import genNewID, compareResults, storeResults
from hide import API_TOKEN
import pandas

logging.basicConfig(level=logging.INFO)
# You will have to set up a telegram bot to use this service.
# Get your api token from https://t.me/botfather and set the it to be the API_TOKEN in secrets.py
bot = Bot(token=API_TOKEN)

# For example use simple MemoryStorage for Dispatcher.
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)


# States
class Form(StatesGroup):
    country = State()  # ill be represented in storage as 'Form:country'
    stateProv = State()  # ill be represented in storage as 'Form:stateProv'
    townCity = State()  # Will be represented in storage as 'Form:townCity'


# Start interaction
@dp.message_handler(commands='start')
@dp.message_handler(regexp='start')
async def cmd_start(message: types.Message):
    """
    Conversation's entry point
    """
    # Set state
    await Form.country.set()

 # Country question prep
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    markup.add("ca", "us")
    markup.add("Other")

    await message.reply("What Country do you live in? (currently only supports Canada)", reply_markup=markup)
# ---------- End Start ----------


# You can use state '*' if you need to handle all states
@dp.message_handler(state='*', commands='cancel')
@dp.message_handler(Text(equals='cancel', ignore_case=True), state='*')
async def cancel_handler(message: types.Message, state: FSMContext):
    """
    Allow user to cancel any action
    """

    # Remove keyboard
    #markup = types.ReplyKeyboardRemove()

    current_state = await state.get_state()
    if current_state is None:
        return

    logging.info('Cancelling state %r', current_state)
    # Cancel state and inform user about it
    await state.finish()
    # And remove keyboard (just in case)
    await message.reply('Cancelled.', reply_markup=types.ReplyKeyboardRemove())


    await Form.next()
# ---------- End * State ----------


# ---------- Start Country State ----------
@dp.message_handler(lambda message: message.text not in ["ca", "us", "Other"], state=Form.country)
async def process_country_invalid(message: types.Message):
    # If country invalid
    return await message.reply("Wrong country try using the keyboard.")


@dp.message_handler(lambda message: message.text in ["ca", "us", "Other"], state=Form.country)
async def process_country(message: types.Message, state: FSMContext):
    # Update state and data
    await Form.next()
    await state.update_data(country=message.text)
    async with state.proxy() as data:

        if data['country'] == 'us':
            await message.reply("What state do you live in? (two letter abbreviation)")

        elif data['country'] == 'ca':
            # Configure stateProv keyboard
            markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
            markup.add("BC", "AB", "SK", "MT", "YT", "NT",  "NU", "ON", "QC", "NL", "NB", "NS", "PE")
            await message.reply("What province do you live in? ", reply_markup=markup)
# ---------- End Country State ----------


# ---------- Start StateProv State ----------
@dp.message_handler(lambda message: message.text not in ["BC", "YT","AB", "NT", "SK", "NU", "MT", "ON", "QC"], state=Form.stateProv)
async def process_stateprov_invalid(message: types.Message):
    # If country invalid
    return await message.reply("Invalid state or province; try using the keyboard.")


# Check that stateProv is on the list
@dp.message_handler(state=Form.stateProv)
async def process_stateprov(message: types.Message, state: FSMContext):

    # Update state and data
    await Form.next()
    await state.update_data(stateProv=message.text)

    # Remove keyboard for townCity
    markup = types.ReplyKeyboardRemove()
    await message.reply("What town or city do you live in?", reply_markup=markup)
# ---------- End StateProv State ----------


# ---------- Start TownCity State ----------
# Add input checking
#
#
@dp.message_handler(state=Form.townCity)
async def process_towncity(message: types.Message, state: FSMContext):

    # Update state and data
    await Form.next()
    await state.update_data(townCity=message.text)
    # Remove keyboard
    markup = types.ReplyKeyboardRemove()


    async with state.proxy() as data:

        # error checking reuired here!
        try:
            results = Weather(data['townCity'], data['stateProv'], data['country'])
            if isinstance(results, Weather):
                await Form.country.set()
            # And send message
            await bot.send_message(
                message.chat.id,
                md.text(
                    md.text('Country: ', md.bold(data['country'])),
                    md.text('stateProv:', md.code(data['stateProv'])),
                    md.text('town/City: ', data['townCity']),
                    md.text('Todays Average Temp: ', results._currentWeather),
                    md.text('Average Temp Last 30 Days: ', results._normWeather),
                    md.text('Weather score: ', results._weatherScore),
                    md.text('Higher Scores are better.'),
                    sep='\n',
                ),
            )
            #reply_markup=markup,
            parse_mode=ParseMode.MARKDOWN,

        except ValueError:
            # Send message and return to the starting state.
            await bot.send_message(message.chat.id, "There was an issue with your choice of city.\n Please click /start to try again.")

        finally:
            # Finish conversation
            await state.finish()

# ---------- End TownCity State ----------


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
