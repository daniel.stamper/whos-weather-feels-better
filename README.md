# Whos weather feels better?

If you live somewhere colder than where your friend lives, but you still want to brag about the fact that your weather feels warmer than theirs because it is warmer than normal for you.

This project is written in python.
It relies on the telegram bot api and open-weather api.
The open-weather api dependency is going to be removed shortly in favour of environment canada and noaa datasources.


## Purpose
This project is a small project that I am writing to learn about software development, data management, and the use of third party APIs.

## Installation
setup a python virtual environment
pip install requirements.txt
create a file called hide.py and add openWeather and telegram bot tokens.
- use the format from hide_template.py for this file.

## To run
python fsm.py

This bot is sometimes running in telegram @wwib_bot